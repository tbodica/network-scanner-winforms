﻿namespace WindowsFormsAppPingerNetwork
{
    partial class NetworkScannerMainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewNetwork = new System.Windows.Forms.ListView();
            this.columnHeaderName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderIP = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderStatus = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonScan = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.numericUpDownMinutes = new System.Windows.Forms.NumericUpDown();
            this.periodicCheckBox = new System.Windows.Forms.CheckBox();
            this.periodicLabelUnits = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.infoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.periodicRunningLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinutes)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listViewNetwork
            // 
            this.listViewNetwork.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderName,
            this.columnHeaderIP,
            this.columnHeaderStatus});
            this.listViewNetwork.HideSelection = false;
            this.listViewNetwork.Location = new System.Drawing.Point(9, 26);
            this.listViewNetwork.Margin = new System.Windows.Forms.Padding(2);
            this.listViewNetwork.Name = "listViewNetwork";
            this.listViewNetwork.Size = new System.Drawing.Size(366, 331);
            this.listViewNetwork.TabIndex = 0;
            this.listViewNetwork.UseCompatibleStateImageBehavior = false;
            this.listViewNetwork.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderName
            // 
            this.columnHeaderName.Text = "Name";
            // 
            // columnHeaderIP
            // 
            this.columnHeaderIP.Text = "IP";
            // 
            // columnHeaderStatus
            // 
            this.columnHeaderStatus.Text = "Status";
            // 
            // buttonScan
            // 
            this.buttonScan.Location = new System.Drawing.Point(378, 300);
            this.buttonScan.Margin = new System.Windows.Forms.Padding(2);
            this.buttonScan.Name = "buttonScan";
            this.buttonScan.Size = new System.Drawing.Size(235, 56);
            this.buttonScan.TabIndex = 1;
            this.buttonScan.Text = "Scan";
            this.buttonScan.UseVisualStyleBackColor = true;
            this.buttonScan.Click += new System.EventHandler(this.ButtonScan_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(378, 269);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(232, 26);
            this.progressBar1.TabIndex = 2;
            // 
            // numericUpDownMinutes
            // 
            this.numericUpDownMinutes.Enabled = false;
            this.numericUpDownMinutes.Location = new System.Drawing.Point(380, 26);
            this.numericUpDownMinutes.Name = "numericUpDownMinutes";
            this.numericUpDownMinutes.Size = new System.Drawing.Size(98, 20);
            this.numericUpDownMinutes.TabIndex = 3;
            // 
            // periodicCheckBox
            // 
            this.periodicCheckBox.AutoSize = true;
            this.periodicCheckBox.Location = new System.Drawing.Point(380, 52);
            this.periodicCheckBox.Name = "periodicCheckBox";
            this.periodicCheckBox.Size = new System.Drawing.Size(98, 17);
            this.periodicCheckBox.TabIndex = 4;
            this.periodicCheckBox.Text = "Periodic Check";
            this.periodicCheckBox.UseVisualStyleBackColor = true;
            this.periodicCheckBox.CheckedChanged += new System.EventHandler(this.periodicCheckBox_CheckedChanged);
            // 
            // periodicLabelUnits
            // 
            this.periodicLabelUnits.AutoSize = true;
            this.periodicLabelUnits.Enabled = false;
            this.periodicLabelUnits.Location = new System.Drawing.Point(484, 28);
            this.periodicLabelUnits.Name = "periodicLabelUnits";
            this.periodicLabelUnits.Size = new System.Drawing.Size(75, 13);
            this.periodicLabelUnits.TabIndex = 5;
            this.periodicLabelUnits.Text = "Time (minutes)";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.logToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(622, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logsToolStripMenuItem});
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.logToolStripMenuItem.Text = "Log";
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.logsToolStripMenuItem.Text = "Logs";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.infoToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // infoToolStripMenuItem
            // 
            this.infoToolStripMenuItem.Name = "infoToolStripMenuItem";
            this.infoToolStripMenuItem.Size = new System.Drawing.Size(95, 22);
            this.infoToolStripMenuItem.Text = "Info";
            this.infoToolStripMenuItem.Click += new System.EventHandler(this.infoToolStripMenuItem_Click);
            // 
            // periodicRunningLabel
            // 
            this.periodicRunningLabel.AutoSize = true;
            this.periodicRunningLabel.Location = new System.Drawing.Point(381, 250);
            this.periodicRunningLabel.Name = "periodicRunningLabel";
            this.periodicRunningLabel.Size = new System.Drawing.Size(35, 13);
            this.periodicRunningLabel.TabIndex = 7;
            this.periodicRunningLabel.Text = "label1";
            // 
            // NetworkScannerMainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(622, 366);
            this.Controls.Add(this.periodicRunningLabel);
            this.Controls.Add(this.periodicLabelUnits);
            this.Controls.Add(this.periodicCheckBox);
            this.Controls.Add(this.numericUpDownMinutes);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.buttonScan);
            this.Controls.Add(this.listViewNetwork);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NetworkScannerMainWindow";
            this.Text = "Network Scanner";
            this.Load += new System.EventHandler(this.NetworkScannerMainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinutes)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView listViewNetwork;
        private System.Windows.Forms.ColumnHeader columnHeaderName;
        private System.Windows.Forms.ColumnHeader columnHeaderIP;
        private System.Windows.Forms.ColumnHeader columnHeaderStatus;
        private System.Windows.Forms.Button buttonScan;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.NumericUpDown numericUpDownMinutes;
        private System.Windows.Forms.CheckBox periodicCheckBox;
        private System.Windows.Forms.Label periodicLabelUnits;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem infoToolStripMenuItem;
        private System.Windows.Forms.Label periodicRunningLabel;
    }
}

