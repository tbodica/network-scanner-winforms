﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using System.Net.NetworkInformation;

namespace WindowsFormsAppPingerNetwork
{
    public partial class NetworkScannerMainWindow : Form
    {
        private System.Windows.Forms.Timer periodicTimer;
        NetworkItemRepo networkItems; // working on
        //List<NetworkItem> networkItems2;// try 2
        LogWindow showLogWindow;
        BackgroundWorker backgroundWorker1;
        public NetworkScannerMainWindow()
        {
            InitializeComponent();
            backgroundWorker1 = new BackgroundWorker();
            backgroundWorker1.ProgressChanged += BackgroundWorker1_ProgressChanged;
            backgroundWorker1.WorkerReportsProgress = true;

            networkItems = new NetworkItemRepo();

            periodicRunningLabel.Visible = false;
            periodicRunningLabel.Text = "Running periodic task.";
            periodicRunningLabel.ForeColor = Color.LightGreen;

            showLogWindow = new LogWindow();
        }

        private void NetworkScannerMainWindow_Load(object sender, EventArgs e)
        {
            using(var reader = new StreamReader(@"items_to_scan.txt"))
            {
                List<string> listNames = new List<string>();
                List<string> listIPs = new List<string>();

                int lineCounter = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (lineCounter == 0)
                    {
                        lineCounter += 1;
                        continue;
                    }

                    var values = line.Split(',');

                    listNames.Add(values[0]);
                    listIPs.Add(values[1]);

                    // post repo code
                    NetworkItem file_item;
                    try
                    {
                        file_item = new NetworkItem(values[0], values[1], false);
                        networkItems.Add(file_item);
                    }
                    catch
                    {
                        Console.WriteLine("123!");
                    }

                    lineCounter += 1;
                }

                // region debug
                foreach (var item in listNames)
                    Console.WriteLine(item);

                foreach (var ip in listIPs)
                    Console.WriteLine(ip);
                // end region

                // region populate listview from listNames
                for (int i = 0; i < listNames.Count; ++i)
                {
                    String name = listNames[i];
                    String ip = listIPs[i];
                    ListViewItem item = new ListViewItem(name);
                    item.SubItems.Add(ip);
                    item.SubItems.Add("Not scanned");
                    listViewNetwork.Items.Add(item);
                }
                listViewNetwork.Columns[0].Width = -2;
                listViewNetwork.Columns[1].Width = -2;
                listViewNetwork.Columns[2].Width = -2;
            }
        }

        private void BackgroundWorker1_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void ButtonScan_Click(object sender, EventArgs e)
        {
            if (periodicCheckBox.Checked == false)
            {
                ScanNetwork(this, null);
            }
            else
            {
                if (numericUpDownMinutes.Value == 0)
                {
                    MessageBox.Show(this, "Value (in minutes) must be greater than 0.", "Time Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (periodicTimer != null && periodicTimer.Enabled == true)
                {
                    periodicRunningLabel.Visible = false;
                    periodicTimer.Stop();
                    periodicTimer.Dispose();
                    return;
                }

                int minutesCount = (int)(numericUpDownMinutes.Value);

                periodicTimer = new System.Windows.Forms.Timer();
                periodicTimer.Interval = minutesCount * 60 * 1000;
                //periodicTimer = new System.Timers.Timer(minutesCount * 60 * 1000);
                periodicTimer.Tick += ScanNetwork;
                periodicTimer.Start();
                periodicRunningLabel.Visible = true;

            }
        }

        private void ScanNetwork(Object source, EventArgs e)
        {            
            for (int i = 0; i < listViewNetwork.Items.Count; ++i)
            {
                listViewNetwork.Items[i].BackColor = Color.Aquamarine;
                string itemIP = listViewNetwork.Items[i].SubItems[1].Text;

                Ping pingObject = new Ping();
                PingReply pingReplyObject;
                pingReplyObject = pingObject.Send(itemIP);


                string scan_status = "";


                if (pingReplyObject.Status == IPStatus.Success)
                {
                    listViewNetwork.Items[i].SubItems[2].Text = "OK";
                    scan_status = "OK";
                    listViewNetwork.Refresh();
                }
                else
                {
                    listViewNetwork.Items[i].SubItems[2].Text = "Failed";
                    scan_status = "Failed";
                    listViewNetwork.Refresh();
                }
                listViewNetwork.Items[i].BackColor = Color.White;
                string logOutputLine = "";
                NetworkItem currentItem = networkItems.GetItem(i);

                
                logOutputLine += "At " + DateTime.Now + " | " + currentItem.Alias + " | ip=" + currentItem.Ip_addr + " | scan status: " + scan_status;
                Console.WriteLine(logOutputLine);
                showLogWindow.AddLine(logOutputLine);

                backgroundWorker1.ReportProgress(((i * 100) / (listViewNetwork.Items.Count - 1)));
            }

        }

        private void periodicCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (periodicCheckBox.CheckState == CheckState.Checked)
            {
                numericUpDownMinutes.Enabled = true;
                periodicLabelUnits.Enabled = true;
            }
            else
            {
                numericUpDownMinutes.Enabled = false;
                periodicLabelUnits.Enabled = false;
            }

            if (periodicTimer != null && periodicTimer.Enabled == true)
            {
                periodicRunningLabel.Visible = false;
                periodicTimer.Stop();
                periodicTimer.Dispose();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String Message = "For periodic scan, please view logs for historical info.";
            String caption = "About NetworkScanner";

            MessageBoxButtons buttons = MessageBoxButtons.OK;
            MessageBoxIcon icons = MessageBoxIcon.Information;

            MessageBox.Show(this, Message, caption, buttons, icons);
        }

        private void logsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (periodicCheckBox.Checked == false)
            {
                MessageBox.Show(this, "Periodic check is disabled!", "Nothing to see.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                showLogWindow.Show();
            }
        }
    }
}
