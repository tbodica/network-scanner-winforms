﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsAppPingerNetwork
{
    public class NetworkItem
    {
        private string alias;
        private string ip_addr;
        private bool scanned;

        public string Ip_addr { get { return ip_addr; } set => SetIp(value); }
        public string Alias { get { return alias; } }
        public bool Scanned { get { return scanned;  } }

        public NetworkItem()
        {
            alias = "";
            ip_addr = "";
            scanned = false;
        }


        public void SetIp(string ip_addr)
        {
            char c;
            for (int i = 0; i < ip_addr.Length; ++i)
            {
                c = ip_addr[i];
                if (c != '.' && ( c > '9' || c < '0'))
                {
                    throw new InvalidOperationException("Bad formatting.");
                }

            }

            this.ip_addr = ip_addr;
        }

        public NetworkItem(string Alias, string ip_addr, bool Scanned)
        {
            this.alias = Alias;
            SetIp(ip_addr);
            this.scanned = Scanned;
        }

    }
}
