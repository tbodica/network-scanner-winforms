﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsFormsAppPingerNetwork
{
    class NetworkItemRepo
    {
        List<NetworkItem> repo;

        public NetworkItemRepo()
        {
            repo = new List<NetworkItem>();
        }
        public void Add(NetworkItem item)
        {
            repo.Add(item);
        }

        public void Remove(NetworkItem item)
        {
            repo.Remove(item);
        }

        public NetworkItem GetItem(int index)
        {
            return repo[index];
        }
        public int Length()
        {
            return repo.Count;
        }
    }
}
